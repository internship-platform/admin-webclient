export default function ({ store, redirect }) {
  if (!store.getters['user/isLogged_in']) {
    return redirect('/')
  }
}
