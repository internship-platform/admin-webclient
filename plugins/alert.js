import Vue from 'vue'

export default function () {
  Vue.mixin({
    data() {
      return {
        alerts: {
          'Incorrect Email or Password': {
            msg: 'Incorrect Email or Password',
            type: 'error',
          },
          'User is not found': {
            msg: 'User is not found',
            type: 'warning',
          },
        },
        currentAlert: {},
      }
    },
    methods: {
      extractError(err) {
        if (err && err.graphQLErrors) {
          if (err.graphQLErrors.length > 0) {
            const errObject = err.graphQLErrors[0]

            if (
              errObject.extensions &&
              errObject.extensions.internal &&
              errObject.extensions.internal.error
            ) {
              return errObject.extensions.internal.error.message
            } else {
              return `${err.graphQLErrors[0].message}`
            }
          } else {
            return 'extraction_error'
          }
        } else if (err && err.errors && err.errors.length > 0) {
          return err.errors[0].message
        } else if (typeof err === 'string') {
          return err
        } else {
          return 'unknown_error'
        }
      },
      humanize(err) {
        let info = {}

        const errMsg = this.extractError(err)

        Object.keys(this.alerts).forEach((item) => {
          if (errMsg.includes(item)) {
            info = this.alerts[item]
          }
        })

        if (Object.keys(info).length === 0 && errMsg) {
          info = {
            msg: errMsg,
            type: 'error',
          }
        }

        return info
      },
      toggleAlert(err, slug, type) {
        const alert = this.humanize(err)
        console.error(err, alert.msg)
        const ca = {
          slug: `app-alert-${slug}`,
          ...alert,
        }
        if (type) {
          ca.type = type
        }
        this.currentAlert = ca
      },
    },
  })
}
