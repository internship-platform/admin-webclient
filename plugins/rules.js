import Vue from 'vue'

export default function ({ app }) {
  Vue.mixin({
    props: {
      rules: {
        type: Object,
        default: () => ({
          required: [
            (v) => {
              if (typeof v === 'number') {
                return true
              }
              return !!v || 'Required'
            },
          ],
          pdf: [
            (v) =>
              (v && v.type.includes('pdf')) || 'Only pdf files are allowed',
          ],
          email: [
            (v) =>
              !v ||
              /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
                v
              ) ||
              'Invalid Email Address',
          ],

          password_length: [
            (v) =>
              !v ||
              v.length >= 8 ||
              'Password too short. Password must be atleast 8 characters',
          ],
          ethiopian_phone_number: [
            (v) =>
              !v || /^\+251\d{9}$/.test(v) || 'Invalid Ethiopian Phone Number ',
          ],
          number: [(v) => !v || /^\d+$/.test(v) || 'Only Numbers are allowed'],
        }),
      },
    },
  })
}
