export default function ({ store }) {
  let user = sessionStorage.getItem('user') || 'null'

  user = JSON.parse(user)

  if (user) {
    store.commit('user/setUserData', user)
  }
}
