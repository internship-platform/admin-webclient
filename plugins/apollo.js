import Vue from 'vue'
import VueApollo from 'vue-apollo'
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { split, ApolloLink, from } from 'apollo-link'
import { WebSocketLink } from 'apollo-link-ws'
import { getMainDefinition } from 'apollo-utilities'
import { onError } from 'apollo-link-error'

export default function ({ store, app }) {
  const connectionParams = () => {
    return {
      headers: {
        authorization: store.getters['user/isLogged_in']
          ? `Bearer ${store.getters['user/getToken']}`
          : undefined,
      },
    }
  }

  const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors && graphQLErrors[0].extensions.code === 'invalid-jwt') {
      store.dispatch('user/removeStateA')
      sessionStorage.removeItem('user')
      store.dispatch('alert/alert', {
        slug: 'login',
        error: 'invalid-jwt',
      })
      app.router.replace('/')
      return
    }

    if (networkError) {
      console.log('network error! please check your connection')
      store.dispatch('alert/alert', {
        slug: 'global',
        error: 'network-error',
      })
    }
  })

  const internshipHTTPLink = new HttpLink({
    uri: 'http://localhost:8080/v1/graphql',
  })

  const internshipWSLink = new WebSocketLink({
    uri: 'ws://localhost:8080/v1/graphql',
    options: {
      reconnect: true,
      lazy: true,
      connectionParams,
    },
  })

  let authLink = (operation, forward) => {
    const { headers } = operation.getContext()
    if (headers) {
      delete headers['x-hasura-role']
    }
    if (store.getters['user/isLogged_in']) {
      operation.setContext({
        headers: {
          authorization: `Bearer ${store.getters['user/getToken']}`,
          ...headers,
        },
      })
    }
    return forward(operation)
  }

  const getDefinition = ({ query }) => {
    const definition = getMainDefinition(query)
    return (
      definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
    )
  }

  const internshipLink = split(
    getDefinition,
    internshipWSLink,
    internshipHTTPLink
  )

  authLink = new ApolloLink(authLink)

  const internshipApolloClient = new ApolloClient({
    link: from([errorLink, authLink, internshipLink]),
    cache: new InMemoryCache({
      addTypename: false,
    }),
    connectToDevTools: true,
  })

  const apolloProvider = new VueApollo({
    defaultClient: internshipApolloClient,
    clients: {
      internship: internshipApolloClient,
    },
  })

  Vue.use(VueApollo)

  Vue.mixin({
    apolloProvider,
  })
}
